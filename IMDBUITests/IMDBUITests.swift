//
//  IMDBUITests.swift
//  IMDBUITests
//
//  Created by Akylbek Utekeshev on 2/18/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import XCTest

class IMDBUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSearchBar() {
        let app = XCUIApplication()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Топ 250"]/*[[".cells.staticTexts[\"Топ 250\"]",".staticTexts[\"Топ 250\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Films"].searchFields["Search bar"].tap()

        app.navigationBars.searchFields["Search bar"].typeText("The Godfather")
        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
    }
    
    
    
    func testGenres() {
        let app = XCUIApplication()
        app.tabBars.buttons["Genres"].tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Comedy"]/*[[".cells.staticTexts[\"Comedy\"]",".staticTexts[\"Comedy\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery/*@START_MENU_TOKEN@*/.staticTexts["Ralph Breaks the Internet"]/*[[".cells.staticTexts[\"Ralph Breaks the Internet\"]",".staticTexts[\"Ralph Breaks the Internet\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["Ralph Breaks the Internet"].buttons["BackArrow"].tap()
        app.navigationBars["Comedy"].buttons["BackArrow"].tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Drama"]/*[[".cells.staticTexts[\"Drama\"]",".staticTexts[\"Drama\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        collectionViewsQuery/*@START_MENU_TOKEN@*/.staticTexts["Creed II"]/*[[".cells.staticTexts[\"Creed II\"]",".staticTexts[\"Creed II\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
    }
    
    func testSimilarMovies() {
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Популярные"]/*[[".cells.staticTexts[\"Популярные\"]",".staticTexts[\"Популярные\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Alita: Battle Angel"]/*[[".cells.staticTexts[\"Alita: Battle Angel\"]",".staticTexts[\"Alita: Battle Angel\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["An angel falls. A warrior rises."]/*[[".cells.staticTexts[\"An angel falls. A warrior rises.\"]",".staticTexts[\"An angel falls. A warrior rises.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        tablesQuery/*@START_MENU_TOKEN@*/.buttons["Similar Movies"]/*[[".cells.buttons[\"Similar Movies\"]",".buttons[\"Similar Movies\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["The Matrix Reloaded"]/*[[".cells.staticTexts[\"The Matrix Reloaded\"]",".staticTexts[\"The Matrix Reloaded\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["The Matrix Reloaded"].buttons["BackArrow"].tap()
        app.navigationBars["Films"].buttons["BackArrow"].tap()
        
    }
    
    func testActorsCast() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Топ 250"].tap()
        tablesQuery.staticTexts["Dilwale Dulhania Le Jayenge"].tap()
        tablesQuery.staticTexts["Come Fall In love, All Over Again.."].swipeUp()
        tablesQuery.buttons["Actors Cast"].tap()
        tablesQuery.staticTexts["Simran Singh"].swipeUp()
        app.navigationBars["Dilwale Dulhania Le Jayenge"].buttons["BackArrow"].tap()
        print("Actors Cast test finished")
    }
    
    func testMovieDetails() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Скоро на экранах"]/*[[".cells.staticTexts[\"Скоро на экранах\"]",".staticTexts[\"Скоро на экранах\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(1)
        tablesQuery.element(boundBy: 0).tap()
        app.swipeUp()
        tablesQuery.children(matching: .cell).element(boundBy: 1).children(matching: .other).element.tap()
        sleep(2)
        app.buttons.element(boundBy: 0).tap()
        print("Movie Details test finished")
    }
    
    func testGetListOfMovies() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Популярные"]/*[[".cells.staticTexts[\"Популярные\"]",".staticTexts[\"Популярные\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.swipeUp()
        app.swipeUp()
        app.navigationBars["Films"].buttons["BackArrow"].tap()
        
        sleep(1)
        
        tablesQuery.staticTexts["Скоро на экранах"].tap()
        app.swipeUp()
        app.swipeUp()
        app.navigationBars["Films"].buttons["BackArrow"].tap()
        
        sleep(1)
        
        tablesQuery.staticTexts["Топ 250"].tap()
        app.swipeRight()
        
        print("Movie List test finished")
    }
}
