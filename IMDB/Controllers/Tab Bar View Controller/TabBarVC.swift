import UIKit

class TabBarViewController: UITabBarController {
    
    //    MARK: Properties
    
    
    lazy var genreController: NavigationController = {
        let controller = GenresViewController()
        controller.tabBarItem = UITabBarItem(title: "Main", image: UIImage(named: "Home"), selectedImage: UIImage(named: "HomeS"))
        
        let nvController = NavigationController(rootViewController: controller)
        return nvController
    }()
    
    lazy var movieController: NavigationController = {
        let services = MovieListViewController()
        services.tabBarItem = UITabBarItem(title: "Услуги", image: UIImage(named: "Search"), selectedImage: UIImage(named: "SearchS"))
        
        let nvController = NavigationController(rootViewController: services)
        return nvController
    }()
    
    
    //    MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupControllers()
        tabBar.items?[0].title = "Фильмы".uppercased()
        tabBar.items?[1].title = "Жанры".uppercased()
        
//        tabBar.barTintColor = UIColor(hex: 0x00AA55)
    }
    
    private func setupControllers() {
        viewControllers = [movieController, genreController]
    }
    
}
