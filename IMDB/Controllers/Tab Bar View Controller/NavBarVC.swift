//
//  NavBarVC.swift
//  IMDB
//
//  Created by Akylbek Utekeshev on 2/18/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        interactivePopGestureRecognizer?.delegate = self
        
        navigationBar.setupNavigationAppearance()
        
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController != self.viewControllers.first {
            let backButton = UIBarButtonItem(image: UIImage(named: "BackArrow"), style: .plain, target: self, action: #selector(popViewController(animated:)) )
            viewController.navigationItem.leftBarButtonItem = backButton
        }
        if viewController == MovieListViewController() {
            
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
