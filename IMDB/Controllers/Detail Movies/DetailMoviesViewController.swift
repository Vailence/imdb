
import UIKit
import SnapKit
import Kingfisher
import SwifterSwift
//import SafariServices

class DetailMoviesViewController: UIViewController{
    
    var getMovieIDGenres = Int()
    var getNavTitle = String()

    var detailBackStage = [String]()
    var movieDetailsDict = [String: String]()
    var posterPath = String()

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.register(cellWithClass: MovieDetailsTableViewCell.self)
        tableView.register(cellWithClass: MovieDetailsButtonViewCell.self)
        tableView.tableFooterView = tableFooterView
        return tableView
    }()
    
    lazy var tableFooterView: UICollectionView = {
        let headerSize = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * 0.45)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let view = UICollectionView(frame: headerSize, collectionViewLayout: layout)
        view.register(cellWithClass: MovieDetailsCollectionViewCell.self)
        view.backgroundColor = .white
        view.showsHorizontalScrollIndicator = false
        view.dataSource = self
        view.delegate   = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movieDetailsJSON()
        movieDetailsBackImages()
        setupViews()
        setupConst()
    }

}


extension DetailMoviesViewController {
    func setupViews() {
        self.view.addSubview(tableView)
        self.title = getNavTitle
    }
    
    func setupConst() {
        tableView.snp.makeConstraints{ make in
            make.edges.equalToSuperview()
            
        }
    }

    
    @objc func similarMoviesBtn() {
        let vc = MovieListViewController()
        vc.getMovieID = getMovieIDGenres
        vc.getSimilarMovie()
        self.navigationController?.pushViewController(vc, animated: true)
        }
    
    @objc func actorsCastBtn() {
        let vc = ActorsViewController()
        vc.getMovieID = getMovieIDGenres
        vc.actorJSON()
        vc.getNavTitle = movieDetailsDict["title"]!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
