
import Foundation
import UIKit

struct MovieDetails : Decodable  {
    let budget, id : Int
    let posterPath: String
    let title, originalTitle : String
    let tagline, status, overview : String
    let voteAverage: Double
    let voteCount, runtime: Int
    let releaseDate: String
    
    
    private enum CodingKeys : String, CodingKey {
        case title
        case originalTitle = "original_title"
        case runtime
        case budget
        case id
        case posterPath = "poster_path"
        case tagline

        case status
        case overview
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case releaseDate = "release_date"
    }
}

struct movieBackDrops : Decodable {
    let backdrops : [Backdrop]
}

struct Backdrop: Codable {
    let filePath: String
    
    enum CodingKeys: String, CodingKey {
        case filePath = "file_path"
    }
}

extension DetailMoviesViewController {
    func movieDetailsJSON() {
        let jsonUrlString = "https://api.themoviedb.org/3/movie/\(self.getMovieIDGenres)?api_key=\(apiKey)&language=ru"
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let result = try JSONDecoder().decode(MovieDetails.self, from: data)
                self.movieDetailsDict["title"] = result.title
                self.movieDetailsDict["originalTitle"] = result.originalTitle

                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "yyyy"
                
                if let date = dateFormatterGet.date(from: result.releaseDate) {
                   self.movieDetailsDict["releaseDate"] = dateFormatterPrint.string(from: date)
                } else {
                    print("There was an error decoding the string")
                }

                let hours = result.runtime / 60
                let minutes = result.runtime % 60

                self.movieDetailsDict["runtime"] = (String(format: "%02d:%02d", hours, minutes))
                self.movieDetailsDict["tagline"] = result.tagline
                self.movieDetailsDict["overview"] = result.overview
                
                self.posterPath = result.posterPath

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            catch let jsonError {
                print("Error" , jsonError)
            }
        } .resume()
    }

    func movieDetailsBackImages() {
        let jsonUrlString = "https://api.themoviedb.org/3/movie/\(self.getMovieIDGenres)/images?api_key=\(apiKey)"
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let result = try JSONDecoder().decode(movieBackDrops.self, from: data)
                for i in result.backdrops {
                    self.detailBackStage.append(i.filePath)
                }
                DispatchQueue.main.async {
                    self.tableFooterView.reloadData()
                }
            }
            catch let jsonError {
                print("Error" , jsonError)
            }
            
            } .resume()
    }
}
