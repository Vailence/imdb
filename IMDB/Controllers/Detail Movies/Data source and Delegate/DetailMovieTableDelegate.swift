
import Foundation
import UIKit

extension DetailMoviesViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return tableView.size.height/2.7
        }
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withClass: MovieDetailsTableViewCell.self, for: indexPath)
            cell.title.text = movieDetailsDict["title"]
            cell.originalTitle.text = "\(movieDetailsDict["originalTitle"] ?? "Blank") (\(movieDetailsDict["releaseDate"] ?? ""))"
            cell.runtime.text = movieDetailsDict["runtime"]
            cell.tagline.text = movieDetailsDict["tagline"]
            let url = URL(string: "https://image.tmdb.org/t/p/w500/\(self.posterPath)")
            cell.filmImage.kf.setImage(with: url)

            return cell
        }
        else {
            let cell2 = tableView.dequeueReusableCell(withClass: MovieDetailsButtonViewCell.self, for: indexPath)
            cell2.overview.text = movieDetailsDict["overview"]
            cell2.similarFilms.addTarget(self, action: #selector(similarMoviesBtn), for: .touchUpInside)
            cell2.actorsCast.addTarget(self, action: #selector(actorsCastBtn), for: .touchUpInside)
            return cell2
        }
    }
}
