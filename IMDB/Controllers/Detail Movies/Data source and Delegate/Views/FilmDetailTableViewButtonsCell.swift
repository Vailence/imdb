
import Foundation
import UIKit
import SnapKit

class MovieDetailsButtonViewCell : UITableViewCell {

    lazy var overview : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.label13Properties()
        return label
    }()
    
    lazy var actorsCast : UIButton = {
        let button = UIButton()
        button.setTitle("Actors Cast", for: .normal)
        button.blueBtnProperty()
        return button
    }()
    
    lazy var similarFilms : UIButton = {
        let button = UIButton()
        button.setTitle("Similar Movies", for: .normal)
        button.blueBtnProperty()
        return button
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension MovieDetailsButtonViewCell {
    private func setupViews() {
        self.addSubviews([overview, actorsCast ,similarFilms])
    }
    
    private func setConstraints() {
        overview.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        actorsCast.snp.makeConstraints { make in
            make.top.equalTo(overview.snp.bottom).offset(10)
            make.left.equalTo(overview)
            make.bottom.equalToSuperview().offset(-10)
        }
        
        similarFilms.snp.makeConstraints { make in
            make.top.bottom.equalTo(actorsCast)
            make.right.equalTo(overview)
            make.left.equalTo(actorsCast.snp.right).offset(5)
            make.width.equalTo(actorsCast)
        }
    }
}
