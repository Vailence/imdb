
import Foundation
import UIKit
import SnapKit

class MovieDetailsTableViewCell : UITableViewCell {
    
    lazy var title : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x0A3055)
//        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.font = UIFont(name: "AvenirNext-Medium", size: 17)
        return label
    }()
    
    lazy var originalTitle : UILabel = {
        let label = UILabel()
//        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.textColor = UIColor(hex: 0x0A3055)
        label.font = UIFont(name: "AvenirNext-Medium", size: 15)
        return label
    }()
    
    lazy var runtime : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x0A3055)
        label.font = UIFont(name: "AvenirNext-Medium", size: 13)
        return label
    }()

    
    lazy var tagline : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x0A3055)
        label.font = UIFont(name: "AvenirNext-Medium", size: 13)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var filmImage : UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 6
        image.layer.masksToBounds = true
        return image
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension MovieDetailsTableViewCell {
    private func setupViews() {
        self.addSubviews([title, originalTitle, runtime, filmImage, tagline])
    }
    
    private func setConstraints() {
        filmImage.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(6)
            make.bottom.equalToSuperview().offset(-3)
            make.width.equalTo(filmImage.snp.height).dividedBy(1.4)
        }
        
        title.snp.makeConstraints { (make) in
            make.top.equalTo(filmImage.snp.top).offset(3)
            make.left.equalTo(filmImage.snp.right).offset(10)
            make.right.equalToSuperview().offset(-8)
        }
        
        originalTitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(4)
            make.left.right.equalTo(title)
        }
        
        runtime.snp.makeConstraints{ make in
            make.top.equalTo(originalTitle.snp.bottom).offset(10)
            make.left.right.equalTo(originalTitle)
        }
        
        tagline.snp.makeConstraints { make in
            make.top.equalTo(runtime.snp.bottom).offset(8)
            make.left.equalTo(filmImage.snp.right).offset(10)
            make.right.equalToSuperview().offset(-8)
        }
    }
}
