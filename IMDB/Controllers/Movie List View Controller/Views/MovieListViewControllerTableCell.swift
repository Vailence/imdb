
import Foundation
import UIKit
import SnapKit

class MovieListViewControllerTableCell : UITableViewCell {
    lazy var title : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x0A3055)
        label.font = UIFont(name: "AvenirNext-Medium", size: 17)
        return label
    }()
    
    lazy var originalTitle : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x0A3055)
        label.font = UIFont(name: "AvenirNext-Medium", size: 15)
        return label
    }()
    
    lazy var voteAverage : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x1E824C)
        label.font = UIFont(name: "AvenirNext-Medium", size: 15)
        return label
    }()
    
    lazy var voteCount : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: 0x696969)
        label.font = UIFont(name: "AvenirNext-Medium", size: 13)
        return label
    }()
    
    lazy var filmImage : UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension MovieListViewControllerTableCell {
    private func setupViews() {
        self.addSubviews([title, originalTitle, filmImage, voteAverage, voteCount])
    }
    
    private func setupConstraints() {
        filmImage.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
//            make.size.equalTo(CGSize(width: 60, height: 88))
            make.width.equalTo(filmImage.snp.height).dividedBy(1.4)
        }

        title.snp.makeConstraints { (make) in
            make.top.equalTo(filmImage.snp.top).offset(7)
            make.left.equalTo(filmImage.snp.right).offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        originalTitle.snp.makeConstraints{ make in
            make.top.equalTo(title.snp.bottom).offset(10)
            make.left.right.equalTo(title)
        }
        
        voteAverage.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-7)
            make.left.equalTo(filmImage.snp.right).offset(16)
            make.width.equalTo(22)
        }
        
        voteCount.snp.makeConstraints { make in
            make.centerY.equalTo(voteAverage)
            make.left.equalTo(voteAverage.snp.right).offset(6)
            make.right.equalTo(title)
        }

    }
}
