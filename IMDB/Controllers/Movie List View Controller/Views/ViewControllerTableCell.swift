//
//  ViewControllerTableCell.swift
//  IMDB
//
//  Created by Akylbek Utekeshev on 2/16/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class MovieListViewControllerTableCell : UITableViewCell {
    lazy var title : UILabel = {
        let label = UILabel()
        label.label13Properties()
        return label
    }()
    
    lazy var vote_avg : UILabel = {
        let label = UILabel()
        label.label17Properties()
        return label
    }()

    
    lazy var overview : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.label13Properties()
        return label
    }()
    
    lazy var filmImage : UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension MovieListViewControllerTableCell {
    private func setupViews() {
        self.addSubview(title)
        self.addSubview(filmImage)
        self.addSubview(overview)
        self.addSubview(vote_avg)
    }
    
    private func setupConstraints() {
        
        filmImage.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.size.equalTo(CGSize(width: 120, height: 177))
        }

        
        title.snp.makeConstraints { (make) in
            make.top.equalTo(filmImage.snp.top).offset(5)
            make.left.equalTo(filmImage.snp.right).offset(16)
            make.right.equalToSuperview().offset(-10)
//            make.height.equalTo(22)
            make.width.lessThanOrEqualTo(500)
        }
        
        vote_avg.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(10)
            make.left.equalTo(filmImage.snp.right).offset(16)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(20)
        }
        
        overview.snp.makeConstraints { (make) in
            make.top.equalTo(vote_avg.snp.bottom).offset(10)
            make.left.equalTo(title)
            make.right.equalTo(vote_avg)
            make.bottom.equalTo(filmImage).offset(-10)
        }
        
        
    }
}
