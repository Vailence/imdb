//
//  MovieListJson.swift
//  IMDB
//
//  Created by Akylbek Utekeshev on 2/18/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import Foundation
import UIKit
import SwifterSwift


struct MovieList : Decodable {
    let total_pages: Int
    let results : [Results]
}

struct Results: Codable {
    let voteCount, id: Int
    let video: Bool
    let voteAverage: Double
    let title: String
    let popularity: Double
    let posterPath: String
    let originalTitle: String
    let genreIDS: [Int]
    let backdropPath: String?
    let adult: Bool
    let overview, releaseDate: String
    
    enum CodingKeys: String, CodingKey {
        case voteCount = "vote_count"
        case id, video
        case voteAverage = "vote_average"
        case title, popularity
        case posterPath = "poster_path"
        case originalTitle = "original_title"
        case genreIDS = "genre_ids"
        case backdropPath = "backdrop_path"
        case adult, overview
        case releaseDate = "release_date"
    }
}

extension MovieListViewController {
    func getMovieList() {
        let jsonUrlString = "https://api.themoviedb.org/3/movie/\(self.getFilmType)?api_key=\(apiKey)&language=en-US&page=\(self.pageNumber)&language=ru"
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            
            do {
                let resultss = try JSONDecoder().decode(MovieList.self, from: data)
                for i in resultss.results {
                    self.codableData.append(i)
                }

                self.currentCodableData = self.codableData
                self.totalPageNumber = resultss.total_pages
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
                
            catch let jsonError {
                print("Error" , jsonError)
            }
        } .resume()
    }
    
    
    func getSimilarMovie() {
        let jsonUrlString = "https://api.themoviedb.org/3/movie/\(self.getMovieID)/similar?api_key=\(apiKey)"
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let resultss = try JSONDecoder().decode(MovieList.self, from: data)
                for i in resultss.results {
                    self.codableData.append(i)
                }
                self.currentCodableData = self.codableData

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            catch let jsonError {
                print("Error" , jsonError)
            }
        } .resume()
    }
}
