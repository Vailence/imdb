import Foundation
import UIKit

extension MovieListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentCodableData.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height/6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieListViewControllerTableCell
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy"
        
        if let date = dateFormatterGet.date(from: currentCodableData[indexPath.row].releaseDate) {
            cell.originalTitle.text = "\(currentCodableData[indexPath.row].originalTitle) (\(dateFormatterPrint.string(from: date)))"
        } else {
            print("There was an error decoding the string")
        }
        
        cell.title.text = currentCodableData[indexPath.row].title
        
        let url = URL(string: "https://image.tmdb.org/t/p/w400/\(currentCodableData[indexPath.row].posterPath)")
        
        cell.filmImage.kf.setImage(with: url)
        cell.voteAverage.text = String(currentCodableData[indexPath.row].voteAverage)
        cell.voteCount.text = "\(currentCodableData[indexPath.row].voteCount.kFormatted)"
        cell.accessoryType = .disclosureIndicator
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = DetailMoviesViewController()
        vc.getMovieIDGenres = currentCodableData[indexPath.row].id
        vc.getNavTitle = currentCodableData[indexPath.row].title
        self.navigationController?.pushViewController(vc)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == currentCodableData.count - 1) && (pageNumber < totalPageNumber)  {
            pageNumber += 1
            getMovieList()
        }
    }
}



extension MovieListViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentCodableData = codableData
            tableView.reloadData()
            return
        }
        
        currentCodableData = codableData.filter({ datas -> Bool in
            return datas.title.contains(searchText)
        })
        
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
        tableView.reloadData()
    }
    
}
