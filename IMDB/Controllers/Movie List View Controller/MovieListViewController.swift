import UIKit
import SnapKit

let apiKey = "02da584cad2ae31b564d940582770598"


class MovieListViewController: UIViewController {
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))

    var getMovieID = Int()
    var pageNumber = 1
    var totalPageNumber = Int()

    var codableData: [Results] = []
    var currentCodableData = [Results]()
    
    var getFilmType = "popular"
    
    private lazy var segmentControl: UISegmentedControl = {
        let items               = ["POPULAR", "BEST", "SOON", "NOW"]
        let segmentedController = UISegmentedControl(items: items)
        let frame = UIScreen.main.bounds
        segmentedController.frame                = CGRect(x: 0, y: 0, width: view.bounds.width, height: 40 )
        segmentedController.selectedSegmentIndex = 0
        segmentedController.addTarget(self, action: #selector(changeViews(sender:)), for: .valueChanged)
        segmentedController.addUnderlineForSelectedSegment()
        return segmentedController
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate   = self
        tableView.tableHeaderView = segmentControl
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.separatorColor = UIColor(hex: 0x4F5A65)
        tableView.register(MovieListViewControllerTableCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        getMovieList()
        setupViews()
        setupConst()
        setupSearchBar()
        }

    func setupSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = "Search bar"
        self.navigationItem.titleView = searchBar
    }
    
    func setupViews() {
        self.view.addSubview(tableView)
    }
    
    func setupConst() {
        tableView.snp.makeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
    
    @objc func changeViews(sender: UISegmentedControl) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            self.segmentControl.changeUnderlinePosition()
            getFilmType = "popular"
            pageNumber = 1
            self.codableData = []
            getMovieList()
        case 1:
            self.segmentControl.changeUnderlinePosition()
            getFilmType = "top_rated"
            pageNumber = 1
            self.codableData = []
            getMovieList()
        case 2:
            self.segmentControl.changeUnderlinePosition()
            getFilmType = "upcoming"
            pageNumber = 1
            self.codableData = []
            getMovieList()
        case 3:
            self.segmentControl.changeUnderlinePosition()
            getFilmType = "now_playing"
            pageNumber = 1
            self.codableData = []
            getMovieList()
        default:
            break
        }
    }
    
}
