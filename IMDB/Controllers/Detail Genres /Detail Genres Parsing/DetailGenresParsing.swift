//
//  DetailGenresParsing.swift
//  IMDB
//
//  Created by Akylbek Utekeshev on 2/18/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import Foundation
import UIKit

struct GenresMovieList : Decodable {
    let total_pages: Int
    let results : [GenresResults]
}

struct GenresResults: Decodable {
    let vote_average : Double
    let title: String
    let id: Int
    let poster_path: String
    let overview: String
}

extension DetailGenresViewController {
    func getMovieList() {
        let jsonUrlString = "https://api.themoviedb.org/3/discover/movie?api_key=\(apiKey)&with_genres=\(self.mutableGenreID)&page=\(pageNumber)"
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            
            do {
                let resultss = try JSONDecoder().decode(GenresMovieList.self, from: data)
                for i in resultss.results {
                    self.genresMovieList.append(i)
                }
                
                self.totalPageNumber = resultss.total_pages
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            catch let jsonError {
                print("Error" , jsonError)
            }
            } .resume()
    }
}

