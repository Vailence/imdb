
import UIKit
import SnapKit
import Kingfisher

class DetailGenresViewController: UIViewController {
    
    var mutableGenreID = Int()
    var genreName = String()
    var genresMovieList: [GenresResults] = []
    
    var pageNumber = 1
    var totalPageNumber = Int()
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.dataSource = self
        view.delegate = self
        view.showsVerticalScrollIndicator = false
        view.register(cellWithClass: DetailGenresCollectionCell.self)
        view.setCollectionViewLayout(layout, animated: true)
        view.backgroundColor = .white
        return view
    }()
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMovieList()
        setupViews()
        setupConst()
    }
    
    func setupViews() {
        self.view.addSubview(collectionView)
        self.view.backgroundColor = .white
        self.title = genreName.firstUppercased
    }

    func setupConst() {
        collectionView.snp.makeConstraints{ (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide).offset(5)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide)
            make.left.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-5)
        }
    }
}
