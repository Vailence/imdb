
import UIKit
import SnapKit

class DetailGenresCollectionCell: UICollectionViewCell {
    lazy var name : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
//        label.adjustsFontSizeToFitWidth = true
        label.label17Properties()
        return label
    }()
    
    lazy var filmImage : UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.addSubviews([name, filmImage])
    }
    
    func setupConstraints() {
        filmImage.snp.makeConstraints{ make in
            make.top.left.right.equalToSuperview()
        }
        
        name.snp.makeConstraints{ make in
            make.top.equalTo(filmImage.snp.bottom).offset(10)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
        }
    }
}
