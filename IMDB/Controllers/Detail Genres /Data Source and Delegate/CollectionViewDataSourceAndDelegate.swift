
import Foundation
import UIKit

extension DetailGenresViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genresMovieList.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: DetailGenresCollectionCell.self, for: indexPath)
        
        cell.name.text = genresMovieList[indexPath.row].title
        let url = URL(string: "https://image.tmdb.org/t/p/w500/\(genresMovieList[indexPath.row].poster_path)")
        cell.filmImage.kf.setImage(with: url)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.deselectItem(at: indexPath, animated: true)
        
        let vc = DetailMoviesViewController()
        vc.getMovieIDGenres = genresMovieList[indexPath.row].id
        vc.getNavTitle = genresMovieList[indexPath.row].title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == genresMovieList.count - 1) && (pageNumber < totalPageNumber)  {
            pageNumber += 1
            getMovieList()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/1.3)
    }
}
