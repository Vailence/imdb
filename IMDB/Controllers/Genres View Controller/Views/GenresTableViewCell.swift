import Foundation
import UIKit
import SnapKit

class GenresTableViewCell : UITableViewCell {
    lazy var title : UILabel = {
        let label = UILabel()
        label.label17Properties()
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension GenresTableViewCell {
    private func setupViews() {
        self.addSubview(title)
    }
    
    private func setupConstraints() {
        title.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
}
