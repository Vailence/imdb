
import Foundation
import UIKit


extension GenresViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genresName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GenresTableViewCell
        cell.title.text = genresName[indexPath.row].name.firstUppercased
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc  = DetailGenresViewController()
        vc.mutableGenreID = genresName[indexPath.row].id
        vc.genreName = genresName[indexPath.row].name
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
