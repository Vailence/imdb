//
//  GenresViewControllerParsing.swift
//  IMDB
//
//  Created by Akylbek Utekeshev on 2/18/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import Foundation
import UIKit

struct Genres : Decodable {
    let genres : [GenresList]
}

struct GenresList : Decodable {
    let name : String
    let id : Int
}

extension GenresViewController {
    func genresJSON() {
        let jsonUrlString = "https://api.themoviedb.org/3/genre/movie/list?api_key=\(apiKey)&language=ru"
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let result = try JSONDecoder().decode(Genres.self, from: data)
                for i in result.genres {
                    self.genresName.append(i)
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            catch let jsonError {
                print("Error" , jsonError)
            }
            
            } .resume()
    }
}
