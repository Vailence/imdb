
import UIKit
import SnapKit

class GenresViewController: UIViewController {
    
    var genresName = [GenresList]()

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.register(GenresTableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genresJSON()
        setupViews()
        setupConst()
    }
    
    func setupViews() {
        self.view.addSubview(tableView)
        self.view.backgroundColor = .white
        self.title = "Genres"
    }
    
    func setupConst() {
        tableView.snp.makeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
}
