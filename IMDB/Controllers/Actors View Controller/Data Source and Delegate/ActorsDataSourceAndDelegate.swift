import Foundation
import UIKit


extension ActorsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return actorsData.count
        }
        else {
            return actorsCrew.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Команда"
        }
        else {
            return "Актеры"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActorsTableViewCell
        if indexPath.section == 0 {
            cell.name.text = actorsData[indexPath.row].name
            cell.character.text = actorsData[indexPath.row].character
            
            switch actorsData[indexPath.row].gender {
            case 1:
                cell.gender.text = "Female"
            case 2:
                cell.gender.text = "Male"
            default:
                cell.gender.text = "-"
            }
            
            let url = URL(string: "https://image.tmdb.org/t/p/w500/\(actorsData[indexPath.row].profile_path ?? "1.jpg")")
            cell.photo.kf.setImage(with: url)
            return cell
        }
        else {
            cell.name.text = actorsCrew[indexPath.row].name
            cell.character.text = actorsCrew[indexPath.row].job
            
            switch actorsCrew[indexPath.row].gender {
            case 1:
                cell.gender.text = "Female"
            case 2:
                cell.gender.text = "Male"
            default:
                cell.gender.text = "-"
            }
            
            let url = URL(string: "https://image.tmdb.org/t/p/w500/\(actorsCrew[indexPath.row].profilePath ?? "1.jpg")")
            cell.photo.kf.setImage(with: url)
            return cell
        }
        
    }
}


