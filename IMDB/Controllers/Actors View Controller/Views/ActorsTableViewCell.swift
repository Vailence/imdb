
import Foundation
import UIKit
import SnapKit

class ActorsTableViewCell : UITableViewCell {
    lazy var name : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 19)
        return label
    }()
    
    lazy var character : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    lazy var gender : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    lazy var photo : UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension ActorsTableViewCell {
    private func setupViews() {
        self.addSubviews([name, character, photo, gender])
    }
    
    private func setupConstraints() {
        photo.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.size.equalTo(CGSize(width: 100, height: 150))
        }
        
        name.snp.makeConstraints { (make) in
            make.bottom.equalTo(character.snp.top).offset(-15)
            make.left.equalTo(photo.snp.right).offset(16)
            make.right.equalToSuperview().offset(-10)
            make.width.lessThanOrEqualTo(500)
        }
        
        character.snp.makeConstraints { make in
            make.centerY.equalTo(photo).offset
            make.left.equalTo(photo.snp.right).offset(16)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(20)
        }
        
        gender.snp.makeConstraints { (make) in
            make.top.equalTo(character.snp.bottom).offset(10)
            make.left.equalTo(photo.snp.right).offset(16)
            make.right.equalToSuperview().offset(-10)
            make.width.lessThanOrEqualTo(500)
        }
    }
}
