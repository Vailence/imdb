//
//  ActorsVCParsing.swift
//  IMDB
//
//  Created by Akylbek Utekeshev on 2/18/19.
//  Copyright © 2019 Akke. All rights reserved.
//

import Foundation
import UIKit

struct ActorTopLevel: Codable {
    let id: Int
    let cast: [Cast]
    let crew: [Crew]
}

struct Cast: Codable {
    let castID: Int
    let character, creditID: String
    let gender, id: Int
    let name: String
    let order: Int
    let profile_path: String?
    
    enum CodingKeys: String, CodingKey {
        case castID = "cast_id"
        case character
        case creditID = "credit_id"
        case gender, id, name, order
        case profile_path
    }
}

struct Crew: Codable {
    let creditID, department: String
    let gender, id: Int
    let job, name: String
    let profilePath: String?

    enum CodingKeys: String, CodingKey {
        case creditID = "credit_id"
        case department, gender, id, job, name
        case profilePath = "profile_path"
    }
}

extension ActorsViewController {
    func actorJSON() {
        let jsonUrlString = "https://api.themoviedb.org/3/movie/\(self.getMovieID)/credits?api_key=\(apiKey)"
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let result = try JSONDecoder().decode(ActorTopLevel.self, from: data)
                for i in result.cast {
                    self.actorsData.append(i)
                }
                
                for j in result.crew {
                    self.actorsCrew.append(j)
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            catch let jsonError {
                print("Error" , jsonError)
            }
            } .resume()
    }
}
