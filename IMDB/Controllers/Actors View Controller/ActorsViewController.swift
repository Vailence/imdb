import UIKit
import SnapKit
import Kingfisher

class ActorsViewController: UIViewController {
    var getMovieID = Int()
    var getNavTitle = String()
    var actorsData: [Cast] = []
    var actorsCrew: [Crew] = []
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.register(ActorsTableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        actorJSON()
        setupViews()
        setupConst()
        }

    func setupViews() {
        self.view.addSubview(tableView)
        self.title = getNavTitle
    }
    
    func setupConst() {
        tableView.snp.makeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
}
